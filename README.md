# Diceware

## What does it do?

It produces strong passwords, more specifically, passwords that:

- Have high entropy.
- Are easy to remember.
- Are transferable (e.g. can be shared or used across systems)

## Why?

Because using the actual diceware method is difficult without dice and I'm sat in front of my computer instead of walking to the shops to buy dice.

I'm also hoping to make this an easy way for non-tech folks to generate passwords… making the CLI client first is scratching my own itch :)


## Word files

Use the Gutenburg project's files:

    mkdir assets
    pushd assets
    curl --remote-name http://www.gutenberg.org/files/3201/files/COMMON.TXT

TODO: Get this to use a proper assets dir.