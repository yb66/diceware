# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Unreleased


## [0.0.3] - 4th April 2019

### Added

- Added toggle for zxcvbn info.
- Fixed a bit of formatting, especially for when non-interactive.
- Slightly better instructions.

### Changed


## [0.0.2] - 25th January 2019

### Added

- Able to turn off colour as a switch.
- Allow caller to choose wordlist.

### Changed

- Made 20k the default word list.
- Better organised Gemfile. A gemspec will be next.
- Expanded a bit on the README.
- Bundle the default wordlist.
- Added this changelog!


## [0.0.1] - 25th January 2019

### Added

- First release, working but no tests.
