require 'linenoise'
require 'zxcvbn'
require 'paint/pa'
require_relative '../diceware.rb'

module Diceware
  class CLI
    def initialize number: nil, wordlist:, colour: true, zxcvbn: true, interactive: true
      @filename = Pathname(wordlist)
      @number = number
      @capitalise = true
      @zxcvbn = zxcvbn
      @interactive = interactive
      @generator = Diceware::Generator.new @filename
      @current = @generator.generate_word_list(@number)
      @number = @current.size
      @tester = Zxcvbn::Tester.new
      if colour == false or ENV["NO_COLOR"]
        Paint.mode = 0
      end
      # No :black  or white in the passphrase
      @available = [:red, :green, :yellow, :blue, :magenta, :cyan]
    end



    def format
      output = @current.dup
      if capitalise?
        output.map!{|c| c.capitalize }
      else
        output.map!{|c| c.downcase }
      end
      unless ENV["NO_COLOR"]
        # Never put 2 of the same colour next to each other
        mix = []
        output.size.times do
          avs = @available - [mix.last]
          mix << avs[rand(avs.size)]
        end
        output.replace output.zip(mix).map{|char,c| Paint[ char, c ,:bold ] }
      end
      separator = ""
      if spaces?
        separator = " "
      elsif hyphens?
        separator = "-"
      end
      output.join separator
    end


    def output_table
      pa <<~COMMANDS
        Commands:
          Generation:
            (r)egenerate the whole phrase
            Regenerate a specific word e.g. (1)
            (d)elete specific word e.g "d" or "d3"
            (n)umber of words - #{number}
            (s)huffle
          (f)ormat:
            1. Capitalise      - #{capitalise?}
            2. Use (s)paces    - #{spaces?}
            3. Use (h)yphens   - #{hyphens?}
            4. Change (l)ength - #{length ? length : "none"}
          Stats:
            (z) Toggle zxcvbn stats - #{@zxcvbn}
          Shortcut examples:
            (f1) -> toggle capitalise
            (n5) -> 5 words
            (d3) -> Remove word 3
          (e)xit

      COMMANDS
      header = ("  %-15s" * @number) % (1..@number).to_a
      str = ("%-17s" * @number) % @current
      printf header + "\n"
      printf str + "\n\n"
    end

    attr_reader :number

    def length
      @length
    end

    def capitalise?
      !!@capitalise
    end

    def spaces?
      !!@spaces
    end

    def hyphens?
      !!@hyphens
    end


    def output_combined
      @output = sprintf "%s", format()
      if @interactive
        @output << "\n"
      end
      print @output
    end


    def output_zxcvbn
      shades = Hash[ (1..4).to_a.zip [:red, :orange, :yellow, :green] ]
      tested = @tester.test(@output)
      pa <<~STR

        zxcvbn test score: #{Paint[tested.score, shades[tested.score]]}
        Entropy: #{Paint[tested.entropy, shades[tested.score]]}
        Estimated time to crack: #{Paint[tested.crack_time_display, shades[tested.score]]}
      STR
      tested = nil
    end


    def render
      output_table if @interactive
      output_combined
      output_zxcvbn if @zxcvbn
    end


    def run
      Linenoise.clear_screen
      @current = @generator.generate_word_list(@number)
      render
      return unless @interactive
      while buf = Linenoise.linenoise('> ')
        case buf
          when /^r$/
            Linenoise.clear_screen
            @current = @generator.generate_word_list(@number)
          when /^d\d?$/
            if md = buf.match(/^d(\d)$/)
              num = md[1]
            else
              pa "Type the number of the word you wish to remove"
              num = Linenoise.linenoise('> ')
            end
            @current.delete_at num.to_i - 1
            @number = @current.size
            Linenoise.clear_screen
          when /^r?\d$/
            md = /^r?(\d)$/.match buf
            @current[md[1].to_i - 1] = @generator.generate_word_list(1).first
            Linenoise.clear_screen
          when /^n\d?$/
            if md = buf.match( /^n(\d)$/ )
              num = md[1]
            else
              pa "Type the number of words you wish to use"
              num = Linenoise.linenoise('> ')
            end
            new_length = num.to_i
            old_number = @number
            @number = new_length
            if new_length > old_number
              difference = new_length - old_number
              @current.concat @generator.generate_word_list(difference)
            elsif new_length < old_number
              @current.replace @current[0..new_length-1]
            end
            @number = new_length
            Linenoise.clear_screen
          when /^s$/
            Linenoise.clear_screen
            @current.shuffle!
          when /^f\d?$/
            if md = buf.match(/f(\d)/)
              num = md[1]
            else
              pa "Choose format to toggle"
              num = Linenoise.linenoise('> ')
            end
            case num
              when "1" then @capitalise = !@capitalise
              when "2"
                @spaces = !@spaces
                @hyphens = false
              when "3"
                @hyphens = !@hyphens
                @spaces = false
            end
            Linenoise.clear_screen
          when "z"
            @zxcvbn = !@zxcvbn
            Linenoise.clear_screen
          when "e", "q", "exit", "quit"
            puts "Exiting..."
            exit 0
        end
        render
      end
    end
  end
end