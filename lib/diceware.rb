require 'pathname'
require 'securerandom'

STDOUT.sync = true


module Diceware

  class Assets
    ASSETS_DIR  = Pathname(__dir__).join("../assets")
  end


  class Generator

    DEFAULT_NUMBER_OF_WORDS = 7

    def initialize filename=nil, number: nil
      @filename = filename
      @pn = Pathname(@filename)
      @number = number || DEFAULT_NUMBER_OF_WORDS
      @fh = File.new @pn
      @max = @fh.size
      at_exit {
        @fh.close unless @fh.closed?
      }
    end


    def generate_word_list num=nil
      num ||= @number
      threads = []

      num.times do
        threads << Thread.new do
          pos = SecureRandom.random_number(@max)
          chars = []
          until pos.zero? or (char = @fh.pread( 1, pos )) == "\n"
            pos -= 1
          end 

          until @fh.eof? or (chars << @fh.pread(1, pos)).last == "\n" and chars.size >= 2 do
            pos += 1
          end

          chars.delete_if{|c| c == "\r" or c == "\n" }.join
        end
      end

      threads.map{|th| th.value.chomp }
    end
  end


end